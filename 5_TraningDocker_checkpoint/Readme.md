# Training a model in a Docker container with checkpoint

# 目的
試著運行一次Traning 並進行中斷點設置，查看是否從中斷點恢復後，能夠持續運行且模型是能夠無誤使用
## 可再嘗試
- Global value : 在API 請求計數
- DB : 在Container 之中使用 DB，不外掛 Volume

# 參考資料
* [使用 Docker 部署 TensorFlow 環境](https://tf.wiki/zh_hant/appendix/docker.html)
    * [](https://snippetinfo.net/mobile/media/2506) 
    * [docker hub - tensorflow/tensorflow](https://hub.docker.com/r/tensorflow/tensorflow/)

# 指令操作
## 建立 mnist 的 container
### 拉取 Tensorflow 的環境
```
sudo docker image pull tensorflow/tensorflow:latest-py3
sudo docker image ls
```

### 建立 mnist 的 docker image
```
sudo docker build . -t training_mnist
sudo docker image ls
```

### 啟動 mnist image 的 container 
```
sudo docker run -d --name mnist_container training_mnist
sudo docker ps
```

#### 查看 Log
```
sudo docker logs -f <container id>

```

### 建立 check point
```
sudo docker checkpoint create mnist_container checkpoint_mnist
```
--leave-running
#### 查看 設定 checkpoint 的狀態
```
sudo docker ps -a
```
<!-- sudo docker start --checkpoint checkpoint_mnist mnist_container -->

## 實驗一: 移除 container

目的: 將停止的 Container 移除後，是否可單就 Checkpoint 啟動
### 移除 container
```
sudo docker rm <container id>
sudo docker ps -a #查看 Container 是否還存在
```
### Checkpoint 啟動 
```
sudo docker start --checkpoint checkpoint_mnist mnist_container
=> Error response from daemon: No such container: mnist_container
```

實驗結果: 無法單就Checkpoint 啟動 Container 