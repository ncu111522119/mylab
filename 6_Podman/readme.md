# Podman

# install at Ubuntu
```
# Ubuntu 20.10 and newer
sudo apt-get -y update
sudo apt-get -y install podman
```
## 參考連結
* [Podman 安裝](https://podman.io/docs/installation)

# Check version
```
$ podman -v
podman version 3.4.2
```
## 專案目錄
* 1_whileLoop
    * Dockerfile: Docker file 用於建立 whileLoop.py 的Container YAML
    * whileLoop.py: While Loop Print Number

# 執行專案
## 目的
建立 While Loop Print Number ，設定Checkpoint 並移至另一台機器執行
在此主要建立 
* image
* Container
    * Checkpoint
## 建立 Podman Image using dockerfile
```
$ cd 1_whileLoop/
$ podman build -t whileloop_sleep1 ./
```
## Run ctainer using Podman image
```
podman run -d --name whileLoop_container whileloop_sleep1
```
## 查看 Container Log
```
$ podman logs -f whileLoop_container
.
..
2023-05-31 02:13:43 PM   407
2023-05-31 02:13:44 PM   408
2023-05-31 02:13:45 PM   409
2023-05-31 02:13:46 PM   410
2023-05-31 02:13:47 PM   411
2023-05-31 02:13:48 PM   412
2023-05-31 02:13:49 PM   413
2023-05-31 02:13:50 PM   414
..
.
```

# 建立 Checkpoint
## 注意事項
* 必須使用 **sudo 的 權限** 才可進行操作

## 建立 Podman Image using dockerfile
```
$ cd 1_whileLoop/
$ sudo podman build -t whileloop_sleep1 ./
```

## 執行 Image ，使其變成 container
```
$ sudo podman run -d --name whileLoop_container whileloop_sleep_2
```

## 列出 目前 Container 
```
$ sudo podman ps
CONTAINER ID  IMAGE                              COMMAND               CREATED        STATUS            PORTS       NAMES
b69a4ad731a1  localhost/whileloop_sleep1:latest  python -u whileLo...  9 minutes ago  Up 9 minutes ago              whileLoop_container
```

## Checkpointing the container
```
$ sudo podman container checkpoint <contianer ID>
```

##
```
$ sudo podman logs -f whileLoop_container
```

##
```
$ sudo podman container restore <contianer ID>
```

##
```
$ sudo podman logs -f whileLoop_container
```

##
```
$ sudo podman container checkpoint 1d038b82e554 -e ../2_checkpoint/checkpoint.tar.gz

$ sudo scp ../2_checkpoint/checkpoint.tar.gz renjie@10.52.52.87:/home/renjie/lab/2_Podman-Checkpoint-migrate
```