# C lang | Print Hello world

# 目的
印出 Hello world

# 執行方式
## 編譯 hello.c
```
$ gcc hello.c
```
## 印出輸出內容
```
$ ./a.out
```
hello world!

