# include <stdio.h> // console 的輸出輸入，print 需使用
#include <unistd.h> // fork 需使用

int main(void){
    fork();
    fork();

    printf("PID is %d\n", getpid());

    return 0;
}

// 參考連結: https://openhome.cc/Gossip/CGossip/HelloWorld.html