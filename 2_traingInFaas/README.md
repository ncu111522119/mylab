# Training In FaaS

# 目的
在FaaS 上試跑 Training ，並查看
- 訓練參數
- 訓練方式
- 流程上的修正

# 撰寫流程
## step 1 : 建立資料夾、FaaS 專案初始化
```
$ mkdir traingInFaas && cd traingInFaas

# 建立 FaaS 專案
$ faas-cli new --lang python cnn-mnist-python-openfass
=> 會有三個檔案
-> cnn-mnist-python/handler.py
-> cnn-mnist-python/requirement.txt
-> cnn-mnist-python.yaml
```
## 參考資料
* [Your first OpenFaaS Function with Python](https://docs.openfaas.com/tutorials/first-python-function/)


# Step 2 : 撰寫 mnist 的 Docker image
## 2.1: CNN Model Training
## 2.2 TensorFlow Check Point
```
checkpoint_path = "training_1/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)

# Create a callback that saves the model's weights
cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                 save_weights_only=True,
                                                 verbose=1)
# Train the model with the new callback
model.fit(train_images, 
          train_labels,  
          epochs=10,
          validation_data=(test_images, test_labels),
          callbacks=[cp_callback])  # Pass callback to training

# 这将创建一个 TensorFlow checkpoint 文件集合，这些文件在每个 epoch 结束时更新：
os.listdir(checkpoint_dir)



```
## 參考資料
[TensorFlow: MNIST CNN Tutorial](https://www.kaggle.com/code/amyjang/tensorflow-mnist-cnn-tutorial)
## Step 2: 撰寫 cnn-mnist-python.yaml
```
version: 1.0
provider:
  name: openfaas
  gateway: http://127.0.0.1:8080  # 8080 修改成 11132
functions:
  cnn-mnist-python:
    lang: python
    handler: ./cnn-mnist-python
    image: cnn-mnist-python:latest
```



# 參考資料
* [CNN Mnist Tensorflow案例](https://www.kaggle.com/code/amyjang/tensorflow-mnist-cnn-tutorial)
    * [PyTorch 案例](https://hackmd.io/@lido2370/SJMPbNnKN?type=view)
* [训练期间保存模型（以_checkpoints_形式保存）](https://www.tensorflow.org/tutorials/keras/save_and_load?hl=zh-cn#%E5%9C%A8%E8%AE%AD%E7%BB%83%E6%9C%9F%E9%97%B4%E4%BF%9D%E5%AD%98%E6%A8%A1%E5%9E%8B%EF%BC%88%E4%BB%A5_checkpoints_%E5%BD%A2%E5%BC%8F%E4%BF%9D%E5%AD%98%EF%BC%89)
    * [(tensorflow)模型的儲存(save)、載入(restore)(轉錄)](https://medium.com/ai%E5%8F%8D%E6%96%97%E5%9F%8E/tensorflow-%E6%A8%A1%E5%9E%8B%E7%9A%84%E5%84%B2%E5%AD%98-save-%E8%BC%89%E5%85%A5-restore-%E8%BD%89%E9%8C%84-7cabaecd8252)
    * [tensorflow 細看存檔：checkpoint 篇](https://ithelp.ithome.com.tw/articles/10215274)