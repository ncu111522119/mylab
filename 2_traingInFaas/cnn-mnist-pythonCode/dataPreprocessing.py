import tensorflow as tf
import seaborn as sns # 資料視覺化套件
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import os
# 查看 tensorflow 的版本
print(tf.__version__)

# 資料前處理
## 載入資料
mnist = tf.keras.datasets.mnist
(x_train, y_train), (x_test, y_test) = mnist.load_data()

## 資料集，視覺化長條圖顯示
#sns.countplot(y_train)

## 確認是否有 NaN　的資料
print("np.isnan: x_train = ",np.isnan(x_train).any())

print("np.isnan: x_test = ",np.isnan(x_test).any())

# Normalization and Reshaping
input_shape = (28, 28, 1)
## 資料格式轉換
# print((x_train.shape[0], x_train.shape[1], x_train.shape[2], 1))
x_train=x_train.reshape(x_train.shape[0], x_train.shape[1], x_train.shape[2], 1)
x_test = x_test.reshape(x_test.shape[0], x_test.shape[1], x_test.shape[2], 1)

## 正規化: 讓值試分布於 0.0 ~ 1.0
### 因為灰階圖像格式，為0~255，所以除以255 
x_train=x_train / 255.0
x_test=x_test/255.0

## Label Encoding
y_train = tf.one_hot(y_train.astype(np.int32), depth=10)
y_test = tf.one_hot(y_test.astype(np.int32), depth=10)

# CNN
## 定義模型
batch_size = 64
num_classes = 10
epochs = 5

model = tf.keras.models.Sequential([
    tf.keras.layers.Conv2D(32, (5,5), padding='same', activation='relu', input_shape=input_shape),
    tf.keras.layers.Conv2D(32, (5,5), padding='same', activation='relu'),
    tf.keras.layers.MaxPool2D(),
    tf.keras.layers.Dropout(0.25),
    tf.keras.layers.Conv2D(64, (3,3), padding='same', activation='relu'),
    tf.keras.layers.Conv2D(64, (3,3), padding='same', activation='relu'),
    tf.keras.layers.MaxPool2D(strides=(2,2)),
    tf.keras.layers.Dropout(0.25),
    tf.keras.layers.Flatten(),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dropout(0.5),
    tf.keras.layers.Dense(num_classes, activation='softmax')
])

model.compile(optimizer=tf.keras.optimizers.RMSprop(epsilon=1e-08), loss='categorical_crossentropy', metrics=['acc'])

## Fit the Training Data

class myCallback(tf.keras.callbacks.Callback):
  def on_epoch_end(self, epoch, logs={}):
    if(logs.get('acc')>0.995):
      print("\nReached 99.5% accuracy so cancelling training!")
      self.model.stop_training = True

callbacks = myCallback()


# Check Point 設定
checkpoint_path = "training1/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)
print("checkpoint_dir",checkpoint_dir)
## Create a callback that saves the model's weights
cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                 save_weights_only=True,
                                                 verbose=1)
# Save the weights using the `checkpoint_path` format
model.save_weights(checkpoint_path.format(epoch=0))



history = model.fit(x_train, y_train,
                    batch_size=batch_size,
                    epochs=epochs,
                    validation_split=0.1,
                    callbacks=[cp_callback])#callbacks=[callbacks])

# 預測結果
test_loss, test_acc = model.evaluate(x_test, y_test)
