# Docker Checkpoint

# 實驗動機
想測試Docker Check point 的機制

# 實驗假設
若 Python 以 for 迴圈方式 慢慢執行 ，並包裝成 Docker Container 若中斷，是否能夠接續執行呢??


# ==CRIU== PPA 安裝
```
$ sudo add-apt-repository ppa:criu/ppa

$ sudo apt update

$ sudo apt install criu
```
## 參考資料:
* [CRIU PPA](https://launchpad.net/~criu/+archive/ubuntu/ppa)
    * [ansible ppa intall 流程](https://docs.google.com/presentation/d/1dIt0myFKkHGU_mW6BIC5RnNchcsOdSL1rcRkgrhhkGA/edit#slide=id.g1a078d9d2b1_0_57)
* [docker checkpoint](https://docs.docker.com/engine/reference/commandline/checkpoint/)


# 啟動 Docker Experimental
```
# 修改 daemon.json，新增 "experimental": true
## 注意 ，
$ sudo nano /etc/docker/daemon.json

# 查看 daemon.json 修改狀況
$ cat /etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "experimental": true
}

# 重新啟動 docker
$ systemctl restart docker

# 查看 docker 狀態
$ systemctl status docker
```

# 建立 Docker image
## 1. 建立 ==Dockerfile==
```
# 使用 python 3.8 的 Image
FROM python:3.8-slim-buster

# 指定當前的工作目錄，預設的工作目錄
WORKDIR /forloop

# 把 ./forloop Copy 到 container 的當前目錄
COPY ./forloop .

# 執行 python forloop.py
CMD [ "python","forloop.py" ] 
```
## 2. 建立 Image
```
$ sudo  docker build -t forloop_sleep1 . --no-cache
## 使用 --no-cache 的主要原因，是避免在 Build Docker image 時
```

## 3. 執行 Image
```
$ sudo docker run -d --name <Container name> forloop_sleep1
```

## 4. docker ps 確認執行狀態
```
$ sudo docker ps

or

$ sudo docker ps -a
```

## 5. 將Image push 到 Docker hub
```
$ docker push repositery/image
```


# 進入 Container 環境之中
```
sudo docker exec -it 4b6eb38fd04c /bin/sh
```

## 參考資料
* [Docker 範例](https://hackmd.io/@karta134033/BJJS6RmfH)
    * [使用Dockerfile建置](https://peihsinsu.gitbooks.io/docker-note-book/content/docker-build.html)
    * [WORKDIR 指定工作目录](https://yeasy.gitbook.io/docker_practice/image/dockerfile/workdir)
    * [Dockerfile RUN，CMD，ENTRYPOINT命令区别](https://www.cnblogs.com/fat-girl-spring/p/14769198.html)

# While Loop Print i，Sleep 1
## 建立 Docker image -> Container
* sudo  docker build -t whileloop_sleep1 ./3_Docker_Checkpoint/. --no-cache
* sudo docker run -d --name whileLoop_container whileloop_sleep1
* sudo docker logs -f whileLoop_container

## Docker Stop/ Start State 觀察
```
會從頭執行
```
* sudo docker stop  whileLoop_container
* sudo docker logs -f whileLoop_container
* sudo docker start whileLoop_container
* sudo docker logs -f whileLoop_container

## 建立 Checkpoinrt
```
建立 Checkpoint 會停止
透過 checkpoint 恢復，可以回到設定中斷點的數字狀態
```
* sudo docker checkpoint create whileLoop_container whileLoop_container_checkepoint
* sudo docker logs -f whileLoop_container
* sudo docker start --checkpoint whileLoop_container_checkepoint  whileLoop_container
* sudo docker logs -f whileLoop_container

###   --leave-running
```
建立 Checkpoint，會持續運行
```
* sudo docker checkpoint create whileLoop_container whileLoop_container_checkepoint_running --leave-running
* sudo docker checkpoint ls whileLoop_container

### 若 rm 是否也能夠恢復?
```
No，Checkpoint 也會一併刪除
```
* sudo docker stop whileLoop_container
* sudo docker rm whileLoop_container
* sudo docker run -d --name whileLoop_container whileloop_sleep1
* sudo docker stop whileLoop_container
* sudo docker start --checkpoint whileLoop_container_checkepoint_running whileLoop_container
=> Error response from daemon: checkpoint whileLoop_container_checkepoint_running does not exist for container /whileLoop_container
* sudo docker checkpoint ls whileLoop_container
=> CHECKPOINT NAME

### Docker export
```
stateless
匯出Container <.tar>，再匯入到image 中，再從image 恢復
```
* sudo docker export whileLoop_container > whileLoop_container.tar
* sudo docker logs whileLoop_container
* sudo docker import whileloop_container.tar whileloop_container_image:latest
* sudo docker run -d --name whileLoop_container whileloop_container_image:latest python -u /forloop/forloop.py
* sudo docker logs whileLoop_container

#### docker import / run container error
```

```
* sudo docker run -d --name whileLoop_container whileloop_container_image:latest python -u forloop.py
* sudo docker logs whileLoop_container
python: can't open file 'forloop.py': [Errno 2] No such file or directory

#### docker Checkpoin 匯入 docker container
```
因為 
```
* sudo cp -R /home/renjie/lab/3_Docker_Checkpoint/0_checkpoint/whileLoop_container_checkepoint_running_2 /var/lib/docker/containers/{ContainerID}/checkpoints
* sudo docker start --checkpoint=whileLoop_container_checkepoint_running_2  whileLoop_container
* sudo docker logs -f whileLoop_container

## 參考資料
* [Python 取得系統當前時間](https://shengyu7697.github.io/python-get-current-time-and-date/)
* [複製目錄（cp 指令）](https://www.ibm.com/docs/zh-tw/aix/7.1?topic=directories-copying-cp-command)
* [报Error response from daemon: custom checkpointdir is not supported的错误](https://www.jianshu.com/p/ed88fe8a446d)
  * **--checkpoint-dir** 在 docker start 被移除的替代方案
### 待參考
* [docker集成criu实现热迁移功能的使用方法](http://luqitao.github.io/2019/01/24/Docker_and_criu_migrate_containers/#%E5%88%9B%E5%BB%BAcheckpoint-1)
# 檔案架構 
* forloop/
    * forloop.py : 以每一秒Print 目前數字，有10,000
* Dockerfile 


# 0511 版本更新 23.0.6
* docker checkpoint create  --checkpoint-dir=/whileLoop_0511  --leave-running whileLoop_contain
er whileLoop_container_checkepoint
  * 無效指令  
    * sudo docker checkpoint create whileLoop_container --checkpoint-dir="." whileLoop
_container_ck0511_2

    * docker checkpoint create  --checkpoint-dir=./ --leave-running whileLoop_container whileLoop_container_checkepoint 

* scp -r /whileLoop_0511/whileLoop_ck0511/ renjie@10.52.52.87:/home/renjie/lab/docker_checkpoint_23.0.6/checkpoint

* sudo docker export whileLoop_container > whileLoop_container_0511.tar

* scp whileLoop_container_0511.tar renjie@10.52.52.87:/home/renjie/lab/docker_checkpoint_23.0.6/

* (worker) 