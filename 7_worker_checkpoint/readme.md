# Podman Contianer Stateful Migrate

# 執行指令
## podman checkpoint
```
$ sudo podman run -d --name looper busybox /bin/sh -c \
         'i=0; while true; do echo $i; i=$(expr $i + 1); sleep 1; done'
$ sudo podman ps

$ sudo podman logs <contianer ID>

$ sudo podman container checkpoint <contianer ID> --tcp-established=true --export=/tmp/chkpt_<contianer ID>.tar.gz

$ sudo scp /tmp/chkpt_<containerID>.tar.gz renjie@10.52.52.86:/tmp
```
## 執行 Podman restore
```
$ sudo podman container restore --tcp-established=true -i /tmp/chkpt_<containerID>.tar.gz
```

## 比對 log 接續 Print 狀態
### 在兩台機器上執行
```
$ sudo podman logs -f <containerID>
```


# Error
## Err: runtime not support
* [Crun 更新](https://docs.google.com/presentation/d/10qQc3gbraAmrGj3qjjfmiFuBLzvV_8djR9VjcDkLcvs/edit#slide=id.g22b08a11569_0_9)

## 輸出 錯誤 的 Log
```
$ sudo cat /var/lib/containers/storage/overlay-containers/<container full ID>/userdata/restore.log > restore_<container full ID>.log
```

```
$ snap set lxd criu.enable=true
$ snap install lxd

```
* 參考資料: [Lxd + criu installation on Ubuntu](https://discuss.linuxcontainers.org/t/lxd-criu-installation-on-ubuntu/16691/1) 

## Error: OCI runtime error: CRIU restoring failed -52.  Please check CRIU logfile 
### 錯誤完整說明
Error: OCI runtime error: CRIU restoring failed -52.  Please check CRIU logfile /var/lib/containers/storage/overlay-containers/37d3348618bf148f0b17ff9636cd7b2928afe3eadfb9d966d4399f5568aeda13/userdata/restore.log
### 輸出Log
#### 將Log 輸出至另一個檔案
```
$ cat /var/lib/containers/storage/overlay-containers/<container full ID>/userdata/restore.log > ./restore_<container full ID>.log
```
#### 開啟 restore.log
```
(01.673593) Running post-restore scripts
(01.673602) 	RPC
(01.673694) mnt: Switching to new ns to clean ghosts
(01.674329) Unlock network
(01.674349) Running network-unlock scripts
(01.674353) 	RPC
iptables-restore: line 6 failed
(01.675826) Error (criu/util.c:641): exited, status=1
ip6tables-restore: line 6 failed
(01.676881) Error (criu/util.c:641): exited, status=1
(01.676956) pie: 1: seccomp: Restoring mode 1 flags 0x3 on tid 1 filter 0
(01.678165) pie: 1: seccomp: Restored mode 2 on tid 1
(01.678219) pie: 1: restoring lsm profile (current) changeprofile containers-default-0.44.4
(01.678266) pie: 1: Error (criu/pie/restorer.c:180): can't write lsm profile -2
(01.678293) Force no-breakpoints restore
(01.678313) 646291 was trapped
(01.678322) 646291 (native) is going to execute the syscall 202, required is 15
(01.678340) 646291 was trapped
(01.678347) `- Expecting exit
(01.678360) 646291 was trapped
(01.678365) 646291 (native) is going to execute the syscall 186, required is 15
(01.678375) 646291 was trapped
(01.678378) `- Expecting exit
(01.678388) 646291 was trapped
(01.678392) 646291 (native) is going to execute the syscall 1, required is 15
(01.678355) pie: 1: Error (criu/pie/restorer.c:1928): BUG at criu/pie/restorer.c:1928
(01.678406) 646291 was trapped
(01.678408) `- Expecting exit
(01.678421) Error (compel/src/lib/infect.c:1544): Task 646291 is in unexpected state: b7f
(01.678428) Error (compel/src/lib/infect.c:1550): Task stopped with 11: Segmentation fault
(01.678431) Error (criu/cr-restore.c:2445): Can't stop all tasks on rt_sigreturn
(01.678434) Error (criu/cr-restore.c:2509): Killing processes because of failure on restore.
The Network was unlocked so some data or a connection may have been lost.
(01.678605) Error (criu/mount.c:3674): mnt: Can't remove the directory /tmp/.criu.mntns.kw92Rx: No such file or directory
(01.678611) Error (criu/cr-restore.c:2536): Restoring FAILED.
```

### 主要錯誤: iptables-restore: line 6 failed - Error (criu/util.c:641): exited, status=1
```
(01.673593) Running post-restore scripts
(01.673602) 	RPC
(01.673694) mnt: Switching to new ns to clean ghosts
(01.674329) Unlock network
(01.674349) Running network-unlock scripts
(01.674353) 	RPC
iptables-restore: line 6 failed
(01.675826) Error (criu/util.c:641): exited, status=1
```
###  修正錯誤
#### 更新 Linux Kernel 配置檔案
```
$ sudo vi /boot/config-$(uname -r)
# CONFIG_NETFILTER_XT_MARK 從“m”更改為“y”後
## vi 可用 / 搜尋字串，在按下 Enter
$ source /boot/config-$(uname -r)
```
#### 新增 --tcp-established 
##### Podman Checkpoint
```
$ sudo podman container checkpoint <containerID> --tcp-established=true -e /tmp/checkpoint_<containerID>.tar.gz
$ sudo scp /tmp/checkpoint_<containerID>.tar.gz renjie@10.52.52.86:/tmp
```
##### Podman Restore
```
$ sudo podman container restore --tcp-established=true -i /tmp/checkpoint_<containerID>.tar.gz 
```

#### 重新編譯 Linux Kernel
```
$ tar xvf linux-5.9.6.tar.xz
$ sudo apt-get install git fakeroot build-essential ncurses-dev xz-utils libssl-dev bc flex libelf-dev bison 
$ cd linux-6.4.1
$ cp -v /boot/config-$(uname -r) .config 
$ make 
$ sudo make INSTALL_MOD_STRIP=1 modules_install
$ sudo make install 
$ uname -mrs.

```

##### Error: No rule to make target 'debian/canonical-revoked-certs.pem', needed by 'certs/x509_revocation_list'. Stop.

* 參考資料:[编译内核报错 No rule to make target ‘debian/canonical-certs.pem‘ 或 ‘canonical-revoked-certs.pem‘ 的解决方法](https://blog.csdn.net/m0_47696151/article/details/121574718)

##### Error：couldn‘t find suitable memory target
* 參考資料: [Ubuntu 换内核后报错：couldn‘t find suitable memory target](https://blog.csdn.net/VoisSurTonChemin/article/details/118791198)

#### 參考資料
* [Docker checkpoint: ip6tables-restore failed](https://github.com/checkpoint-restore/criu/issues/813)
    * [CRIU 設定 Linux kernel](https://criu.org/Linux_kernel)
        * [Linux内核编译很简单，6步编译一个自己的内核](https://www.51cto.com/article/663841.html)
        * [The Linux Kernel Archives](https://www.kernel.org/)
    * [CRIU 設定 Configuration files](https://criu.org/Configuration_files)
* [Lxc checkpoint error-ip6tables-restore](https://discuss.linuxcontainers.org/t/lxc-checkpoint-error/16866)
    * 目前沒有解答Q(0629)