# ETCD Practice

# 檔案架構
* etcd: 使用git clone 下來的專案
    * 


# 安裝網址
https://etcd.io/docs/v3.5/install/

## 安裝步驟
```
$ git clone -b v3.5.0 https://github.com/etcd-io/etcd.git
$ cd etcd
$ ./build.sh
$ export PATH="$PATH:`pwd`/bin"
$ etcd --version
```


# 快速開始
https://etcd.io/docs/v3.5/quickstart/

## 基礎操作
```
$ etcd
{"level":"info","ts":"2023-04-17T10:24:09.307+0800","caller":"etcdmain/etcd.go:72","msg":"Running: ","args":["etcd"]}
{.....

$ etcdctl put greeting "Hello, etcd"
```

## IF 